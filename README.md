![picture](ada/logo.png)

# Ada Example 03 - static & dynamic pages - Version 1.0

This is the a bit more complex example of a website with static and dynamic pages (php scripts) built with ADA (Alpha Dust Advanced).

It is merely a showcase project to learn how to create php WebApps with ADA framework more in depth.

## Getting started (Installing / Running)

To start this, place it into a php supporting webserver (Apache, Nginx,...)

You can also use a php docker container, actually it was developed with a fork of [Dockerized PHP](https://github.com/mikechernev/dockerised-php.git).

## Creation of the WebApp

Here I explain what I did to create this project - you can take the project and adapt from here or start from green field:

For creating the project directory see [ada-example-01-static]() steps 1-3 and 5.

1. Create an `index.php` with following content

	<?php
	// include the Ada framework
	require_once('ada/framework.php');
	// actually run it
	$ada = new Ada();
	$ada->printPartialFromConfig('templates.before');

	// print a static content page
	echo $ada->getStaticContentBody();

	$ada->printPartialFromConfig('templates.after');
	?>

2. Create `ada-files/before.html`, `ada-files/after.html`, `ada-files/site-config.json`, `ada-files/content/*.html`, etc. and adapt to your needs.

3. Create more PHP scripts if needed and extend your PHP WebApp

## Prerequisites

* Any modern browser
* PHP development environment (if using docker eg. https://github.com/mikechernev/dockerised-php.git)

## Installing / Running

Place the project directory in your webserver / php dev environment and call it (eg. http://127.0.0.1/).

## Built With

* [PHP 7.3.0](http://www.php.net/)
* [Twig 2.6.0](https://twig.symfony.com/)

## Contributing

No contributing planned since this is a showcase / example project. But you are invited to clone it and build your own static website based upon it.

## Versioning

This project uses a major and a minor version as versioning system. You can find it on top of this file.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) for details.
