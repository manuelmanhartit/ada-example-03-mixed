<?php
// include the Ada framework
require_once('ada/framework.php');
// actually run it
$ada = new Ada();
$ada->printPartialFromConfig('templates.before');

// print a static content page
echo $ada->getStaticContentBody();

$ada->printPartialFromConfig('templates.after');
?>
