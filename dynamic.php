<?php
// include the Ada framework
require_once('ada/framework.php');
// actually run it
$ada = new Ada();
$ada->printPartialFromConfig('templates.before');

// print dynamic content (a link list), and use the ada templating mechanism
echo $ada->getTwigTemplate(getContent(), $ada->getContext());

// example use of context variables
$currentPageName = $ada->getContextValue('currentPage.name');
echo "<p>Example use of ADA context variables (in your php code): $currentPageName</p>";

// example use of custom defined variables
$email = $ada->getContextValue('components.email');
echo "<p>Example use of custom variables defined in site-config.json (in your php code): $email</p>";


$ada->printPartialFromConfig('templates.after');

// print an example link list
function getContent() {
	// actually we could also just fetch the pages from $ada->getContextValue('pages') and iterate through it in php
    return <<<EOT
    <h2>Link List</h2>
    <ul>
	{% if pages|length > 0 and (not (currentPage.hideNavigation)) %}
		{% for item in pages %}
			<li>
			{% set href = item.href %}
			{% if item.name == currentPage.name %}
				{{ item.name|e }}
			{% else %}
				<a href="{{ href|e }}">
					{{ item.name|e }}
				</a>
			{% endif %}
			</li>
		{% endfor %}
	{% endif %}
	</ul>
    <p>Example use of custom variables defined in site-config.json (in your php files template): {{components.email}}</p>
EOT;
}
?>
